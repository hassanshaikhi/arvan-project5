import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
import redis
redis_host = "redis"
redis_port = 6379
redis_password = ""
#app = dash.Dash('usage chart')


app = dash.Dash(__name__, assets_folder='assets',
                assets_url_path='', url_base_pathname='/charts/')




r = redis.StrictRedis(host=redis_host, port=redis_port,
                      password=redis_password, decode_responses=True)





cpu = r.get("cpu_usage")
mem = r.get("mem_usage")
disk = r.get("disk_usage")

text_style = dict(color='#444', fontFamily='sans-serif', fontWeight=300)
plotly_figure = dict(
    data=[dict(x=["cpu usage", "ram usage", "disk usage"], y=[1, 4, 8])])

app.layout = html.Div([
    html.H2('system usage', style=text_style),
    # html.P('Enter a Plotly trace type into the text box, such as histogram, bar, or scatter.', style=text_style),
    dcc.Input(id='text1', placeholder='box', value='bar'),
    dcc.Graph(id='plot1', figure=plotly_figure),
    dcc.Graph(id='plot2', figure=plotly_figure),



])


@app.callback(Output('plot1', 'figure'), [Input('text1', 'value')])
def text_callback(text_input):
    return {'data': [dict(x=["cpu usage", "ram usage", "disk usage"], y=[r.get("node1_cpu_usage"), r.get("node1_mem_usage"),  r.get("node1_disk_usage")], type=text_input)]}


@app.callback(Output('plot2', 'figure'), [Input('text1', 'value')])
def text_callback(text_input):
    return {'data': [dict(x=["cpu usage", "ram usage", "disk usage"], y=[r.get("node2_cpu_usage"), r.get("node2_mem_usage"),  r.get("node2_disk_usage")], type=text_input)]}


#app.config.supress_callback_exceptions = True
app.config.update({
    # remove the default of '/'
    'routes_pathname_prefix': '/charts/',

    # remove the default of '/'
    'requests_pathname_prefix': '/charts/'
})

if __name__ == "__main__":

    app.server.run(host='0.0.0.0', port=80, debug=True)
