import os
import psutil
import redis
redis_host = "redis"
redis_port = 6379
redis_password = ""

def usage():
    cpu_usage = psutil.cpu_percent(4)
    mem_usage = psutil.virtual_memory()[2]
    obj_Disk = psutil.disk_usage('/')
    disk_usage = obj_Disk.percent
    r = redis.StrictRedis(host=redis_host, port=redis_port,
                        password=redis_password, decode_responses=True)
    r.set("node1_cpu_usage", cpu_usage)
    r.set("node1_mem_usage", mem_usage)
    r.set("node1_disk_usage", disk_usage)

while True:
    usage()