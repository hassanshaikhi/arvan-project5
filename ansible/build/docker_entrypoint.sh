#!/bin/bash

set -e

exec python3 /app/exporter.py &
exec python3 /app/app.py
exec /bin/bash