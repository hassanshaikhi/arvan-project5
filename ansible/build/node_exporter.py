import prometheus_client
import time
import psutil

UPDATE_PERIOD = 10
SYSTEM_USAGE = prometheus_client.Gauge('system_usage',
                                       'Hold current system resource usage',
                                       ['resource_type'])

if __name__ == '__main__':
    prometheus_client.start_http_server(8000)

while True:
    SYSTEM_USAGE.labels('CPU').set(psutil.cpu_percent(3))
    SYSTEM_USAGE.labels('Memory').set(psutil.virtual_memory()[2])
    SYSTEM_USAGE.labels('DISK').set(psutil.disk_usage('/').percent)
    time.sleep(UPDATE_PERIOD)
