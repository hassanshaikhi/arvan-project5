import io
import os
import gzip
import re
import sys
import time

codes200 = []
codes400 = []
codes500 = []
pattern = "HTTP/1.1"
#file = "../logs/haproxy_latest.20200901.log.gz"
#start = "2020-09-01"
#end = "2020-09-03"
start = os.environ['START']
end = os.environ['END']
count = 0
start_var = start.replace("-", "")
end_var = end.replace("-", "")


def log(file):
    with gzip.open(file, 'rt') as fin:
        for line in fin:
            if re.search(pattern, line):
                fields = line.strip().split()
                codes = fields[9]
                if re.match('[2][0-9][0-9]', codes):
                    codes200.append(codes)
                if re.match('[4][0-9][0-9]', codes):
                    codes400.append(codes)
                if re.match('[5][0-9][0-9]', codes):
                    codes500.append(codes)


      #   print(len(codes200))
      #   print(len(codes400))
       #  print(len(codes500))
sys.stdout = open('./logs/log.txt', 'w')

while start_var <= end_var:
    start2 = start_var
    for i in range(1, 3):
        end2 = start_var
        file2 = f"/home/logs/haproxy_latest.{start_var}.log.gz"
        log(file2)
      #  print(file2)
        start_var = int(start_var)
        start_var = start_var + 1
        start_var = str(start_var)
      #  print(start2)
    year_start = start2[:4]
    month_start = start2[4:6]
    day_start = start2[6:8]
    year_end = end2[:4]
    month_end = end2[4:6]
    day_end = end2[6:8]
    day_end=int(day_end)
    day_end += 1
    day_end = str(day_end)
    start_date = f"{year_start}-{month_start}-{day_start}"
    end_date = f"{year_end}-{month_end}-{day_end}"
    print(f"{start_date} 00:00:00 {end_date} 00:00:00 code200: {len(codes200)} code400: {len(codes400)} code500: {len(codes500)}")
    codes200.clear()
    codes400.clear()
    codes500.clear()


sys.stdout.close()

while True:
    time.sleep(60)

    
