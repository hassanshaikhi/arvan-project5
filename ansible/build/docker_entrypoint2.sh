#!/bin/bash

set -e

exec python3 /app/exporter2.py &
exec python3 /app/chart.py