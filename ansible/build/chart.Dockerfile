FROM ubuntu:18.04
RUN apt-get update -y && \
    apt-get install -y python3-pip python3
COPY ./docker_entrypoint2.sh / 
RUN chmod 777 /docker_entrypoint2.sh 
WORKDIR /app
COPY ./requirements.txt /app/requirements.txt

RUN pip3 install -r requirements.txt

COPY ./chart.py /app
COPY ./exporter2.py /app

RUN mkdir -p /app/logs

ENTRYPOINT ["/docker_entrypoint2.sh"]



